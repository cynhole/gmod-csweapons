AddCSLuaFile()

DEFINE_BASECLASS( "ent_basecsgrenade" )

function ENT:Initialize()
    if SERVER then
        self:SetModel("models/Weapons/w_eq_fraggrenade_thrown.mdl")
    end
    BaseClass.Initialize(self)
end

function ENT:BounceSound()
    self:EmitSound("HEGrenade.Bounce")
end
